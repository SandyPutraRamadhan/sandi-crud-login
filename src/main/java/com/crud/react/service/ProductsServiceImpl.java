package com.crud.react.service;

import com.crud.react.exeption.InternalErrorException;
import com.crud.react.exeption.NotFoundException;
import com.crud.react.model.Products;
import com.crud.react.repository.ProductsRepository;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProductsServiceImpl implements ProductsService {

    @Autowired
    ProductsRepository productsRepository;
//FireBase
    private static final String DOWNLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/image-react-eeb49.appspot.com/o/%s?alt=media";

    private String imageConverter(MultipartFile multipartFile) {
        try {
            String fileName = getExtention(multipartFile.getOriginalFilename());
            File file = convertToFile(multipartFile, fileName);
            var RESPONSE_URL =uploadFile(file, fileName);
            file.delete();
            return RESPONSE_URL;
        }catch (Exception e) {
            e.getStackTrace();
            throw new InternalErrorException("Error Upload File");
        }
    }
    private File convertToFile(MultipartFile multipartFile, String fileName) throws IOException {
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)){
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }
    @Override
    public Products addProducts(Products products, MultipartFile multipartFile) {
        String img = imageConverter(multipartFile);
        Products addProducts = new Products(products.getNama(), products.getHarga(), img ,products.getDeskripsi());
       return productsRepository.save(addProducts);
    }
    private String uploadFile(File file, String fileName) throws IOException {
        BlobId blobId = BlobId.of("image-react-eeb49.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/ServiceAccount.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }

    private String getExtention(String filename) {
        return filename.split("\\.")[0];
    }

//    End FireBase

    @Override
    public Products getProducts(Long id) {
        var respon = productsRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not Found"));
        try {
            return productsRepository.save(respon);
        }catch (Exception e) {
            throw new InternalErrorException("Kesalahan Memunculkan Data");
        }
    }


    @Override
    public List<Products> getAllProducts() {
        return productsRepository.findAll();
    }

    @Override
    public Products editProducts(Long id, Products products) {
        Products update = productsRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not Founf"));
        update.setNama(products.getNama());
        update.setDeskripsi(products.getDeskripsi());
        update.setImg(products.getImg());
        update.setHarga(products.getHarga());
        return productsRepository.save(update);
    }

    @Override
    public Map<String, Boolean> deleteProducts(Long id) {
        try {
            productsRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
