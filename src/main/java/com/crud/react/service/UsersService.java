package com.crud.react.service;

import com.crud.react.dto.LoginDto;
import com.crud.react.model.Users;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

public interface UsersService {

    Map<String, Object> login(LoginDto loginDto);

    Users getUsers(Long id);

    Users addUsers(Users users);

    List<Users> getAllUsers();

    Users editUsers(Long id , Users users);

    Map<String, Boolean> deleteUsers(Long id);
}
