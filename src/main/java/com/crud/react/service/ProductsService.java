package com.crud.react.service;

import com.crud.react.model.Products;
import com.crud.react.model.Users;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface ProductsService {
    Products getProducts(Long id);

    Products addProducts(Products products, MultipartFile multipartFile);

    List<Products> getAllProducts();

    Products editProducts(Long id , Products products);

    Map<String, Boolean> deleteProducts(Long id);
}
