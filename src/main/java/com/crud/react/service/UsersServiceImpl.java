package com.crud.react.service;

import com.crud.react.dto.LoginDto;
import com.crud.react.enumated.UsersEnum;
import com.crud.react.exeption.InternalErrorException;
import com.crud.react.exeption.NotFoundException;
import com.crud.react.jwt.JwtProvider;
import com.crud.react.model.Users;
import com.crud.react.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserDetailsImpl userDetailsService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtProvider jwtProvider;

        private String authories(String email, String password) {
            try {
                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
            } catch (BadCredentialsException e) {
                throw new InternalErrorException("Email or Password NOT Found");
            }
            UserDetails userDetails = userDetailsService.loadUserByUsername(email);
            return jwtProvider.generateToken(userDetails);
        }

        @Override
        public Map<String, Object> login(LoginDto loginDto) {
            String token = authories(loginDto.getEmail(), loginDto.getPassword());
            Users user = usersRepository.findByEmail(loginDto.getEmail()).get();
            Map<String, Object> response = new HashMap<>();
            response.put("token", token);
            response.put("expired", "15 menit");
            response.put("user", user);
            return response;
        }

        @Override
        public Users getUsers(Long id) {
            var respon = usersRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not Found"));
            try {
                return usersRepository.save(respon);
            } catch (Exception e) {
                throw new InternalErrorException("Kesalahan Memunculkan Data");
            }
        }

        @Override
        public Users addUsers(Users users) {
            String email = users.getEmail();
            users.setPassword(passwordEncoder.encode(users.getPassword()));
            var validasi = usersRepository.findByEmail(email);
            if (users.getRole().name().equals("ADMIN"))
                users.setRole(UsersEnum.ADMIN);
            else users.setRole(UsersEnum.USER);
            if (validasi.isPresent()) {
                throw new InternalErrorException("Email Already Axist");
            }
            return usersRepository.save(users);
        }

        @Override
        public List<Users> getAllUsers() {
            return usersRepository.findAll();
        }

        @Override
        public Users editUsers(Long id, Users users) {
            Users update = usersRepository.findById(id).orElseThrow(() -> new NotFoundException("Users Not Found"));
            var validasi = usersRepository.findByEmail(users.getEmail());
            if (validasi.isPresent()) {
                throw new InternalErrorException("sorry email already exists");
            }
            update.setEmail(users.getEmail());
            update.setPassword(users.getPassword());
            return usersRepository.save(update);
        }

        @Override
        public Map<String, Boolean> deleteUsers(Long id) {
            try {
                usersRepository.deleteById(id);
                Map<String, Boolean> res = new HashMap<>();
                res.put("deleted", Boolean.TRUE);
                return res;
            } catch (Exception e) {
                throw new NotFoundException("Id Not Found");
            }
        }
}
