package com.crud.react.controller;

import com.crud.react.dto.ProductDto;
import com.crud.react.model.Products;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponHelper;
import com.crud.react.service.ProductsService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductsController {

    @Autowired
    ProductsService productsService;

    @Autowired
    ModelMapper modelMapper;

    @PostMapping ( consumes = "multipart/form-data")
    public CommonResponse<Products> addProducts(ProductDto productDto, @RequestPart("file") MultipartFile multipartFile) {
        return ResponHelper.ok(productsService.addProducts(modelMapper.map(productDto, Products.class), multipartFile));
    }
    @GetMapping
    public CommonResponse<List<Products>> getAllProducts() {
        return ResponHelper.ok(productsService.getAllProducts());
    }
    @GetMapping("/{id}")
    public CommonResponse<Products> getProducts(@PathVariable("id") Long id) {
        return ResponHelper.ok(productsService.getProducts(id));
    }

    @PutMapping("/{id}")
    public CommonResponse<Products> editProducts(@PathVariable("id") Long id, @RequestBody ProductDto productDto) {
        return ResponHelper.ok(productsService.editProducts(id, modelMapper.map(productDto, Products.class)));
    }
    @DeleteMapping("/{id}")
    public CommonResponse<?> deleteProducts(@PathVariable("id") Long id) {
        return ResponHelper.ok(productsService.deleteProducts(id));
    }
}
