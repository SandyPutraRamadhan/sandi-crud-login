package com.crud.react.controller;

import com.crud.react.dto.LoginDto;
import com.crud.react.dto.UsersDto;
import com.crud.react.model.Users;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponHelper;
import com.crud.react.service.UsersService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UsersController {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    UsersService usersService;

    @PostMapping("/sign-in")
    public CommonResponse<Map<String, Object>> login (@RequestBody LoginDto loginDto) {
        return ResponHelper.ok(usersService.login(loginDto));
    }
    @PostMapping("/sign-up")
    public CommonResponse<Users> addUsers(@RequestBody UsersDto usersDto) {
        return ResponHelper.ok(usersService.addUsers(modelMapper.map(usersDto, Users.class)));
    }

    @GetMapping
    public CommonResponse<List<Users>> getAllUsers() {
        return ResponHelper.ok(usersService.getAllUsers());
    }
    @PutMapping("/{id}")
    public CommonResponse<Users> editUsers(@PathVariable("id") Long id, @RequestBody Users users) {
        return ResponHelper.ok(usersService.editUsers(id, users));
    }
    @DeleteMapping("/{id}")
    public CommonResponse<?> deleteUsers(@PathVariable("id") Long id) {
        return ResponHelper.ok(usersService.deleteUsers(id));
    }
}
