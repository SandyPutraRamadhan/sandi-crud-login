package com.crud.react.model;

import javax.persistence.*;

@Entity
@Table(name = "products")
public class Products {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama")
    private String nama;

    @Lob
    @Column(name = "deskripsi")
    private String deskripsi;

    @Lob
    @Column(name = "img")
    private String img;

    @Column(name = "harga")
    private Integer harga;

    public Products() {
    }

    public Products(String nama, Integer harga, String img, String deskripsi) {
        this.nama = nama;
        this.deskripsi = deskripsi;
        this.img = img;
        this.harga = harga;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Integer getHarga() {
        return harga;
    }

    public void setHarga(Integer harga) {
        this.harga = harga;
    }

    @Override
    public String toString() {
        return "Products{" +
                "id=" + id +
                ", nama='" + nama + '\'' +
                ", deskripsi='" + deskripsi + '\'' +
                ", img='" + img + '\'' +
                ", harga=" + harga +
                '}';
    }
}
