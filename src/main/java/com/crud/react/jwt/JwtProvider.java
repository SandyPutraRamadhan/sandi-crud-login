package com.crud.react.jwt;

import com.crud.react.exeption.NotFoundException;
import com.crud.react.model.TemporaryToken;
import com.crud.react.model.Users;
import com.crud.react.repository.TemporaryTokenRepository;
import com.crud.react.repository.UsersRepository;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
public class JwtProvider {
    private static String seccetKey = "belajar spring";

    private static Integer expired = 900000;

    @Autowired
   private UsersRepository usersRepository;

    @Autowired
    TemporaryTokenRepository temporaryTokenRepository;

    public String generateToken(UserDetails userDetails) {
        String token = UUID.randomUUID().toString().replace("", "");
        Users user = usersRepository.findByEmail(userDetails.getUsername()).orElseThrow(() -> new NotFoundException("User Not Found Generate Token"));
        var chekingToken = temporaryTokenRepository.findByUserId(user.getId());
        if (chekingToken.isPresent()) temporaryTokenRepository.findByUserId(chekingToken.get().getId());
        TemporaryToken temporaryToken = new TemporaryToken();
        temporaryToken.setToken(token);
        temporaryToken.setExpiredDate(new Date(new Date().getTime() + expired));
        temporaryToken.setUserId(user.getId());
        temporaryTokenRepository.save(temporaryToken);
        return token;
    }
    public TemporaryToken getSubject(String token) {
        return temporaryTokenRepository.findByToken(token).orElseThrow(() -> new NotFoundException("Token Error Parse"));
    }

    public boolean checkingTokenJwt(String token) {
        TemporaryToken tokenExist = temporaryTokenRepository.findByToken(token).orElse(null);
        if (tokenExist == null) {
            System.out.println("Token Kosong");
            return false;
        }
        if (tokenExist.getExpiredDate().before(new Date())) {
            System.out.println("Token Expired");
            return false;
        }
        return true;
    }
}

